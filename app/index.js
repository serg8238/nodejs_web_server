const express = require('express')
const app = express()
const fs = require('fs')
const path = './visits.txt';
const {PORT, HOST} = global.process.env;
let counterState;

app.listen(parseInt(PORT), HOST, () => {
    init();
    console.log(`App listening at http://${HOST}:${PORT}`);
});

app.get('/', (req, res) => {
    incrementCounter();
    res.send(`Visit counts: ${counterState}`);
})

function init() {
    if (fs.existsSync(path)) {
        counterState = parseInt(fs.readFileSync(path).toString());
    } else {
        fs.writeFileSync(path, '0');
        counterState = 0;
    }
}

function incrementCounter() {
    counterState += 1;
    fs.writeFile(path, `${counterState}`, (err) => {
        if (err) console.log(err);
    });
}